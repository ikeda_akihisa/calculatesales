package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_NAMES_NOT_SEQUENTIAL = "売上ファイル名が連番になっていません";
	private static final String DIGIT_OVERFLOW = "合計金額が10桁を超えました";
	private static final String ANY_BRANCH_CODE_NOT_FOUND = "の支店コードが不正です";
	private static final String ANY_COMMODITY_CODE_NOT_FOUND = "の商品コードが不正です";
	private static final String ANY_FILE_INVALID_FORMAT= "のフォーマットが不正です";
	private static final String BRANCH_DEFINITION = "支店定義";
	private static final String COMMODITY_DEFINITION = "商品定義";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// コマンドライン引数が1つ以外の設定であればエラー
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイルの正規表現
		String branchRegex = "^[0-9]{3}$";
		String commodityRegex = "^[0-9a-zA-Z]{8}$";
		// 商品定義ファイルの正規表現

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_DEFINITION, branchRegex)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY_DEFINITION, commodityRegex)) {
			return;
		}

		// ファイル(非ディレクトリ)かつ売上ファイルのみrcdFilesに追加
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length ; i++) {
			String file = files[i].getName();
			if(files[i].isFile() && file.matches("^[0-9]{8}\\.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		// 売上ファイルを番号順に並べ替える
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			// 番号に飛びがあればエラー
			if(latter - former != 1) {
				System.out.println(FILE_NAMES_NOT_SEQUENTIAL);
				return;

			}
		}

		// 売上ファイルを読み込む
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			File file = rcdFiles.get(i);
			try {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;

				// 一行ずつ読み込んだ値を配列に入れる
				List<String> saleData = new ArrayList<>();
				while((line = br.readLine()) != null) {
					saleData.add(line);
				}
				// 売上ファイルのフォーマットが不正ならエラー
				if(saleData.size() != 3) {
					System.out.println(file.getName() + ANY_FILE_INVALID_FORMAT);
				}
				String branchCode = saleData.get(0);
				String commodityCode = saleData.get(1);
				// 読み込んだ支店コードが存在しなければエラー
				if(!branchSales.containsKey(branchCode)) {
					System.out.println(file.getName() + ANY_BRANCH_CODE_NOT_FOUND);
					return;
				}
				// 読み込んだ商品コードが存在しなければエラー
				if(!commoditySales.containsKey(commodityCode)) {
					System.out.println(file.getName() + ANY_COMMODITY_CODE_NOT_FOUND);
					return;
				}
				// 売上金額が数字であるか確認
				if(!saleData.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				// ファイル内の売上金額
				long fileSale = Long.parseLong(saleData.get(2));
				// 支店別合計売上金額の計算
				long branchSaleAmount = branchSales.get(branchCode) + fileSale;
				// 商品別合計売上金額の計算
				long commoditySaleAmount = commoditySales.get(commodityCode) + fileSale;
				// 支店別集計結果が仕様の桁を超えればエラー
				if(branchSaleAmount >= 10000000000L) {
					System.out.println(DIGIT_OVERFLOW);
					return;
				}
				// 商品別集計結果が仕様の桁を超えればエラー
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println(DIGIT_OVERFLOW);
					return;
				}
				// 合計金額を再設定
				branchSales.put(branchCode, branchSaleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);
			} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
			} finally {
				if(br != null ) {
					try {
					// ファイルを閉じる
					br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}


		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String fileType, String regex) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}

			// 定義ファイル読み込み
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2 || (!items[0].matches(regex)))) {
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;
				}

				// 支店コード、支店名、売上金額をそれぞれのマップにキーバリューを追加
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// 支店別集計ファイル書き込み
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			// 1行ずつ書き込む
			for(String code : names.keySet()) {
				bw.write(code + "," + names.get(code) + "," + sales.get(code));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
